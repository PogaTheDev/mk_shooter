﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class PlayerInput : MonoBehaviour
    {
        [HideInInspector] public Player player;

        public Vector3 leftInputVector;
        public Vector3 rightInputVector;

        public VirtualStick LeftVirtualStick;
        public VirtualStick RightVirtualStick;

        private void Awake()
        {
            player = GetComponent<Player>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        public void UpdateInput()
        {
            UpdateConsoleInput();
            UpdateTouchInput();
        }

        public void UpdateConsoleInput()
        {
            leftInputVector = new Vector3(Input.GetAxis("LeftHorizontal"),0, Input.GetAxis("LeftVertical"));
            rightInputVector = new Vector3(Input.GetAxis("RightHorizontal"),0, Input.GetAxis("RightVertical"));
        }

        public void UpdateTouchInput()
        {
            leftInputVector += LeftVirtualStick.inputVector;
            rightInputVector += RightVirtualStick.inputVector;
        }

        
    }
}


