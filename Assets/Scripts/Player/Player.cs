﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class Player : MonoBehaviour
    {
        [HideInInspector] public PlayerInput playerInput;
        [HideInInspector] public PlayerMovement playerMovement;
        [HideInInspector] public PlayerShooting playerShooting;
        [HideInInspector] public PlayerAnimations playerAnimations;

        public Transform ProjectileAnchor;
        public bool Alive;

        private void Awake()
        {
            //Link Player Components
            playerInput = GetComponent<PlayerInput>();
            playerMovement = GetComponent<PlayerMovement>();
            playerShooting = GetComponent<PlayerShooting>();
            playerAnimations = GetComponent<PlayerAnimations>();
        }

        // Start is called before the first frame update
        void Start()
        {
            Alive = true;
        }

        // Update is called once per frame
        void Update()
        {
            
            UpdateInput();
            if (GameDirector.inst.gameState == GameDirector.GameState.InGame)
            {
                UpdateMovement();
                UpdateShooting();
                UpdateAnimations();
            }

        }

        private void LateUpdate()
        {
            LateUpdateMovement();
        }

        void UpdateInput()
        {
            playerInput.UpdateInput();
        }

        void UpdateMovement()
        {
            playerMovement.UpdateMovement();
        }

        void UpdateShooting()
        {
            if (playerInput.RightVirtualStick.stickActive || playerInput.rightInputVector.sqrMagnitude > 0)
            {
                playerShooting.UpdateShooting();
            }
            else
            {
                playerShooting.ResetShotTimer();
            }
            
        }

        void UpdateAnimations()
        {
            playerAnimations.UpdateAnimations();
        }

        void LateUpdateMovement()
        {
            playerMovement.LateUpdateMovement();
        }

        public void Die()
        {
            if (Alive)
            {
                playerAnimations.DieAnimation();
                Alive = false;
                Debug.Log("Died");
                GameDirector.inst.EndGame();
            }

        }


    }
}

