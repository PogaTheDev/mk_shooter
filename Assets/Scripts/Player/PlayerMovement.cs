﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class PlayerMovement : MonoBehaviour
    {
        [HideInInspector] public Player player;
        [HideInInspector] public CharacterController cc;

        public float MoveSpeed;
        public float ShootingMoveSpeed;

        public Vector3 Velocty;
        public Vector3 previousPosition;

        public float rotateLerpSpeed;

        private void Awake()
        {
            player = GetComponent<Player>();
            cc = GetComponent<CharacterController>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        public void UpdateMovement()
        {
            UpdatePosition();
            UpdateRotation();
        }

        public void UpdatePosition()
        {
            Vector3 moveVector = Vector3.zero;

            //Update player position
            if (player.playerShooting.IsShooting)
            {
                moveVector = player.playerInput.leftInputVector * ShootingMoveSpeed * Time.deltaTime;
            }
            else
            {
                moveVector = player.playerInput.leftInputVector * MoveSpeed * Time.deltaTime;
            }

            //Apply Gravity
            moveVector += Physics.gravity * Time.deltaTime;

            cc.Move(moveVector);

            //Update Velocity
            Velocty = transform.position - previousPosition;
        }

        public void UpdateRotation()
        {
            if(player.playerInput.rightInputVector.sqrMagnitude > 0)
            {
                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.playerInput.rightInputVector), rotateLerpSpeed * Time.deltaTime);
            }
            else
            {
                if(Velocty.sqrMagnitude > 0)
                {
                    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(player.playerInput.leftInputVector), rotateLerpSpeed * Time.deltaTime);
                }
                
            }
            
        }

        public void LateUpdateMovement()
        {
            previousPosition = transform.position;
        }

    }
}

