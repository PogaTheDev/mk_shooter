﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class PlayerAnimations : MonoBehaviour
    {

        [HideInInspector] public Player player;

        public Animator animator;

        public const string MoveSpeed = "MoveSpeed";
        public const string IsShooting = "IsShooting";
        public const string Die = "Die";

        private void Awake()
        {
            player = GetComponent<Player>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }

        public void UpdateAnimations()
        {
            UpdateAnimationVariables();
        }

        public void UpdateAnimationVariables()
        {
            
            animator.SetFloat(MoveSpeed, player.playerMovement.Velocty.magnitude);

            animator.SetBool(IsShooting, player.playerShooting.IsShooting);
        }

        public void DieAnimation()
        {
            animator.SetTrigger(Die);
        }
    }
}

