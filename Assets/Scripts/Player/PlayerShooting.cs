﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class PlayerShooting : MonoBehaviour
    {
        [HideInInspector] public Player player;

        public GameObject ProjectilePrefab_Basic;

        public float ShotCooldown;
        public float ShotTimer;

        public bool IsShooting;

        private void Awake()
        {
            player = GetComponent<Player>();
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        public void UpdateShooting()
        {
            IsShooting = true;

            if (ShotTimer > 0)
            {
                ShotTimer -= Time.deltaTime;
            }
            else
            {
                Shoot();
                ShotTimer = ShotCooldown;
            }
        }

        public void ResetShotTimer()
        {
            IsShooting = false;

            ShotTimer = ShotCooldown;
        } 

        public void Shoot()
        {
            //Create a projectile
            GameObject newProjectile = Instantiate<GameObject>(ProjectilePrefab_Basic);

            //Direct it forward relative to the player
            newProjectile.GetComponent<Projectile>().Initialize(player.ProjectileAnchor.position, transform.rotation, transform.forward);
        }
    }
}

