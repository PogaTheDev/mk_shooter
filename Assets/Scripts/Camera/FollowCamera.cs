﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class FollowCamera : MonoBehaviour
    {
        #region Tweaking Variables
        //The distance the camera attempts to lead infront of the characters facing direciton
        public float cameraLeadDistance;
        //The speed the camera lerps towards its target postion
        public float followLerpSpeed;
        #endregion

        #region Tracking Variables
        //Use to snapshot the initial distance between the character and the camera
        Vector3 InitialOffset;
        //The calculated position the camera is moving towards
        Vector3 targetPosition;
        #endregion

        #region Internal Component References
        public Transform player;
        #endregion

        #region Functions
        void Start()
        {
            InitialOffset = transform.position - player.position;
        }

        void Update()
        {
            UpdatePosition();
        }

        void UpdatePosition()
        {
            targetPosition = player.position + InitialOffset + (player.forward * cameraLeadDistance);
            transform.position = Vector3.LerpUnclamped(transform.position, targetPosition, followLerpSpeed * Time.deltaTime);
        }
        #endregion
    }
}

