﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace MKShooter
{
    public class GameDirector : MonoBehaviour
    {
        #region Tweaking Variables
        public float GameOverTimeSlow;
        #endregion

        #region Tracking Variables
        public int Score;
        public enum GameState
        {
            PreGame,
            InGame,
            PostGame
        }
        public GameState gameState;
        #endregion

        #region Internal Component References
        public static GameDirector inst;
        #endregion

        #region External Component References
        public Player player;
        public GameObject WelcomeInfo;
        public GameObject GameOverInfo;
        public TMP_Text ScoreText;
        #endregion

        #region Functions
        void Awake()
        {
            //Initialize Singlton
            inst = this;

            //Local player object
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }

        void Start()
        {
            gameState = GameState.PreGame;
            UpdateScore(0);
        }

        void Update()
        {
            switch (gameState)
            {
                case GameState.PreGame:
                    //If the player is giving any kind of character input
                    if (player.playerInput.leftInputVector.magnitude != 0 || player.playerInput.rightInputVector.magnitude != 0)
                    {
                        StartGame();
                    }
                    break;
                case GameState.InGame:
                    break;
                case GameState.PostGame:
                    break;
            }
        }

        public void StartGame()
        {
            gameState = GameState.InGame;
            WelcomeInfo.SetActive(false);
        }

        public void EndGame()
        {
            gameState = GameState.PostGame;
            GameOverInfo.SetActive(true);
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(0);
        }

        public void UpdateScore(int _Adjustment)
        {
            Score += _Adjustment;
            ScoreText.text = Score.ToString();
        }
        #endregion



    }
}

