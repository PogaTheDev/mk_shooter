﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualStick : MonoBehaviour
{

    public Vector3 inputVector;

    public RectTransform SpawnBoundary;
    public RectTransform ClampBoundary;
    public GameObject Stick;
    public RectTransform InnerStick;
    public RectTransform OuterStick;

    public bool stickActive;
    public int currentFingerID;
    public Vector3 touchPosition;

    public float StickLimits;

    

    public float DeadZonePercent;

    Vector3 StartingPosition;

    public float StickReturnLerpSpeed;

    //Tracks if the stick has been moved outside the dead zone
    public bool Directed;

    public bool AnchorLocked;

    // Start is called before the first frame update
    void Start()
    {
        StartingPosition = Stick.transform.position;
        DeactiveStick();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateStickMovement();
        UpdatePlayerInput();
    }

    public void UpdateStickMovement()
    {
        //If the stick isnt active
        if (stickActive == false)
        {
            //Loop through every current touch
            for (int i = 0; i < Input.touchCount; i++)
            {
                //If a touch has just begun
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    Touch touch = Input.GetTouch(i);
                    //Check if it is inside the boundary
                    if (touch.position.x < SpawnBoundary.position.x + (SpawnBoundary.rect.width / 2) &&
                       touch.position.x > SpawnBoundary.position.x - (SpawnBoundary.rect.width / 2) &&
                       touch.position.y < SpawnBoundary.position.y + (SpawnBoundary.rect.height / 2) &&
                       touch.position.y > SpawnBoundary.position.y - (SpawnBoundary.rect.height / 2))
                    {
                        //Assign the touch to current tough and activate the stick
                        ActivateStick(touch.fingerId, touch.position);
                    }
                }
            }

            //Reset the position
            Stick.transform.position = Vector3.Lerp(Stick.transform.position, StartingPosition, StickReturnLerpSpeed * Time.deltaTime);
            InnerStick.localPosition = Vector3.Lerp(InnerStick.localPosition, Vector3.zero, StickReturnLerpSpeed * Time.deltaTime);
        }

        if (stickActive)
        {
            //Loop through every current touch
            for (int i = 0; i < Input.touchCount; i++)
            {
                Touch touch = Input.GetTouch(i);

                //It the touch is the same finger
                if (touch.fingerId == currentFingerID)
                {
                    //If the touch phase is releasing
                    if (touch.phase == TouchPhase.Ended)
                    {
                        DeactiveStick();
                    }
                    else
                    {
                        //Update the position
                        touchPosition = touch.position;

                        //Check where you wanna move the stick, if it is too far away from the anchor, it clamps it to the limit
                        Vector3 TargetPosition = touchPosition - Stick.transform.position;

                        if (TargetPosition.magnitude >= StickLimits)
                        {
                            
                            if (AnchorLocked)
                            {
                                
                            }
                            else
                            {
                                Vector3 targetOuterPosition = touchPosition - (TargetPosition.normalized * StickLimits);
                                //targetOuterPosition.x = Mathf.Clamp(targetOuterPosition.x, ClampBoundary.position.x - (ClampBoundary.rect.width/2) + (OuterStick.rect.width/2), ClampBoundary.position.x + (ClampBoundary.rect.width/2) - (OuterStick.rect.width/2));
                                //targetOuterPosition.y = Mathf.Clamp(targetOuterPosition.y, ClampBoundary.position.y - (ClampBoundary.rect.height/2) + (OuterStick.rect.height/2), ClampBoundary.position.y + (ClampBoundary.rect.height/2)- (OuterStick.rect.height/2));

                                //To moving the anchor
                                Stick.transform.position = Vector3.Lerp(Stick.transform.position, targetOuterPosition, StickReturnLerpSpeed * Time.deltaTime);
                            }

                            //Change this clamp of the stick
                            TargetPosition = TargetPosition.normalized * StickLimits;

                        }

                        InnerStick.transform.localPosition = Vector3.Lerp(InnerStick.transform.localPosition, TargetPosition, StickReturnLerpSpeed * Time.deltaTime);

                        //Check sto see if the right stick has been moved outside of its dead zone, to start transmitting an input vector
                        if(Directed == false)
                        {
                            if (TargetPosition.magnitude > DeadZonePercent * StickLimits)
                            {
                                Directed = true;
                            }
                        }
                        
                    }

                }
            }
        }
    }

    void UpdatePlayerInput()
    {
        if(stickActive && Directed)
        {
            Vector3 StickDelta = (InnerStick.position - Stick.transform.position) / StickLimits;
            inputVector = new Vector3(StickDelta.x, 0, StickDelta.y);
        }
        else
        {
            inputVector = Vector3.zero;
        }
    }

    void ActivateStick(int _currentFingerID, Vector3 _anchorPosition)
    {
        
        stickActive = true;
        //Stick.SetActive(true);
        currentFingerID = _currentFingerID;

        //Update the position
        //Stick.transform.position = _anchorPosition;
    }

    void DeactiveStick()
    {
        Directed = false;
        stickActive = false;
        //Stick.SetActive(false);

        //Reset the position
        //Stick.transform.position = StartingPosition;
        //InnerStick.localPosition = Vector3.zero;
    }
}
