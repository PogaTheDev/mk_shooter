﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float duration;

    public Rigidbody rb;
    public Vector3 direction;
    public float speed;

    public virtual void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Start is called before the first frame update
    public virtual void Start()
    {
        //Destroy the projectile after its duration expires
        Destroy(this.gameObject, duration);
    }

    // Update is called once per frame
    public virtual void Update()
    {
        UpdateMovement();
    }

    public virtual void Initialize(Vector3 _origin, Quaternion _rotation)
    {
        transform.position = _origin;
        transform.rotation = _rotation;
        rb.velocity = transform.forward * speed;
    }

    public virtual void Initialize(Vector3 _origin, Quaternion _rotation, Vector3 _direction)
    {
        transform.position = _origin;
        transform.rotation = _rotation;
        direction = _direction;
        rb.velocity = direction * speed;
    }

    public virtual void Initialize(Vector3 _origin, Quaternion _rotation, Vector3 _direction, float _speed)
    {
        transform.position = _origin;
        transform.rotation = _rotation;
        direction = _direction;
        speed = _speed;
        rb.velocity = direction * speed;
    }

    public virtual void UpdateMovement()
    {
        
    }

    public virtual void Explode()
    {
        //Destroy the projectile after its duration expires
        Destroy(this.gameObject);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag != "Player")
        {
            Explode();
        }
    }



}
