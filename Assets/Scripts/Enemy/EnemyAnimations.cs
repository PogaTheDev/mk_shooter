﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class EnemyAnimations : MonoBehaviour
    {
        [HideInInspector] public Enemy enemy;

        public Animator animator;

        public const string MoveSpeed = "MoveSpeed";
        public const string Attack = "Attack";
        public const string Die = "Die";

        private void Awake()
        {
            enemy = GetComponent<Enemy>();
        }

        public void UpdateAnimations()
        {
            UpdateAnimationVariables();
        }

        public void UpdateAnimationVariables()
        {
            animator.SetFloat(MoveSpeed, enemy.Velocity.magnitude);

            //animator.SetTrigger(Att, player.playerShooting.IsShooting);
        }

        public void AttackAnimation()
        {
            animator.SetTrigger(Attack);
        }

        public void DieAnimation()
        {
            animator.SetTrigger(Die);
        }
    }
}

