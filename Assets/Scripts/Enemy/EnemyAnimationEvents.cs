﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class EnemyAnimationEvents : MonoBehaviour
    {
        public Enemy enemy;

        public void StartAttack()
        {
            enemy.StartAttack();
        }

        public void FinishAttack()
        {
            enemy.FinishAttack();
        }
    }
}

