﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace MKShooter
{
    public class Enemy : MonoBehaviour
    {
        #region Tweaking Variables
        //Base movement speed of enemies
        public float BaseSpeed;
        //The time after "dying" that it takes for the object to dissappear
        public float DeathDelay;
        //The time it takes the enemy to spawn in on its pedestal
        public float SpawnDelay;
        public float AttackRange;
        public float AttackCooldown;
        #endregion

        #region Tracking Variables
        public float AttackingSpeed;
        public Vector3 Velocity;
        public Vector3 PreviousPosition;
        public float AttackTimer;
        bool Alive;
        #endregion

        #region Internal Component References
        public EnemyAnimations enemyAnimtions;
        NavMeshAgent agent;
        public BoxCollider enemyCollider;
        #endregion

        #region External Component References
        public Transform objectToFollow;
        #endregion

        #region Functions
        private void Awake()
        {
            enemyAnimtions = GetComponent<EnemyAnimations>();
            enemyCollider = GetComponent<BoxCollider>();
            agent = GetComponent<NavMeshAgent>();
        }

        void Start()
        {
            Alive = true;
            PreviousPosition = transform.position;
        }

        void Update()
        {
            if (GameDirector.inst.gameState == GameDirector.GameState.InGame)
            {
                UpdateDestination();
                UpdateAttack();
                UpdateAnimations();
            }
            else
            {
                agent.enabled = false;
            }
        }

        void UpdateDestination()
        {
            if (objectToFollow != null)
            {
                if (agent.enabled)
                {
                    agent.SetDestination(objectToFollow.position);
                }
            }
        }

        void UpdateAnimations()
        {
            enemyAnimtions.UpdateAnimations();
        }

        void UpdateAttack()
        {
            if (AttackTimer > 0)
            {
                AttackTimer -= Time.deltaTime;
            }
            else
            {
                if (objectToFollow != null)
                {
                    if (Alive)
                    {
                        //Debug.Log(Vector3.Distance(transform.position, objectToFollow.position));
                        if (Vector3.Distance(transform.position, objectToFollow.position) < AttackRange)
                        {

                            Attack();
                        }
                    }
                }
            }
        }

        void ResetAttackTimer()
        {
            AttackTimer = AttackCooldown;
        }

        public void Initialize(Vector3 _position, Quaternion _rotation)
        {
            transform.position = _position;
            transform.rotation = _rotation;

            StartCoroutine(DelayedSpawn(SpawnDelay));

        }

        void FindPlayer()
        {
            if (GameObject.FindGameObjectWithTag("Player") != null)
            {
                objectToFollow = GameObject.FindGameObjectWithTag("Player").transform;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.GetComponent<Projectile>() != null)
            {
                Debug.Log("Hit Projectile");
                Die();
            }
            else
            {
                Debug.Log("Collided with: " + collision.gameObject.name);
            }
        }

        private void Attack()
        {
            if (objectToFollow.gameObject.GetComponent<Player>().Alive)
            {
                enemyAnimtions.AttackAnimation();
            }
            ResetAttackTimer();
        }

        private void Die()
        {
            enemyAnimtions.DieAnimation();

            GetComponent<Collider>().enabled = false;
            agent.enabled = false;
            Alive = false;

            GameDirector.inst.UpdateScore(1);

            Destroy(this.gameObject, DeathDelay);
        }

        private void LateUpdate()
        {
            Velocity = transform.position - PreviousPosition;
            PreviousPosition = transform.position;
        }

        public void StartAttack()
        {
            Debug.Log("Starting attack");
            agent.speed = AttackingSpeed;
        }

        public void FinishAttack()
        {
            Debug.Log("Finishing Attack");
            agent.speed = BaseSpeed;

            objectToFollow.gameObject.GetComponent<Player>().Die();
        }
        #endregion

        #region Coroutines
        IEnumerator DelayedSpawn(float _Delay)
        {
            GetComponent<Collider>().enabled = false;
            Alive = false;

            yield return new WaitForSeconds(_Delay);

            agent.speed = BaseSpeed;
            FindPlayer();
            GetComponent<Collider>().enabled = true;
            Alive = true;
        }
        #endregion

    }
}

