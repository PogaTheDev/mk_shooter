﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MKShooter
{
    public class EnemySpawn : MonoBehaviour
    {
        public GameObject EnemyPrefab;

        public int EnemySpawnCounter;

        public float BaseSpawnCooldown;

        public float SpawnTimer;

        public float MinSpawnCooldown;

        public float RandomSpawnOffset;

        public float ESC_SpawnTimeFactor;

        // Start is called before the first frame update
        void Start()
        {
            ResetSpawnTimer();
        }

        // Update is called once per frame
        void Update()
        {
            if (GameDirector.inst.gameState == GameDirector.GameState.InGame)
            {
                UpdateSpawnTimer();
            }
        }

        void UpdateSpawnTimer()
        {
            if(SpawnTimer > 0)
            {
                SpawnTimer -= Time.deltaTime;
            }
            else
            {
                SpawnEnemy();
                ResetSpawnTimer();
            }
        }

        void SpawnEnemy()
        {
            GameObject newEnemy = GameObject.Instantiate(EnemyPrefab);
            newEnemy.GetComponent<Enemy>().Initialize(transform.position, transform.rotation);

            EnemySpawnCounter++;
        }

        private void ResetSpawnTimer()
        {
            SpawnTimer = Mathf.Clamp(BaseSpawnCooldown + Random.Range(0, RandomSpawnOffset) - (EnemySpawnCounter * ESC_SpawnTimeFactor), MinSpawnCooldown, Mathf.Infinity);
        }
    }
}

